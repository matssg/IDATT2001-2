# 2 - Arv og polymorfi

### Problemstilling

Et flyselskap tilbyr tre ulike typer bonuskort. En kunde tjener opp poeng hver gang han reiser med fly. Når en kunde melder seg inn i ordningen starter han alltid som medlem på nivå Basic.

Dersom kunden reiser ofte og tjener opp mange poeng det første året han er medlem i ordningen oppgraderes han automatisk til nivå Sølv eller Gull.

For å bli sølvmedlem må kunden tjene opp 25.000 poeng i løpet av ett år. Kravet til gullmedlemskap er 75.000 poeng. 

En kunde er enten basic-medlem, sølv-medlem eller gull-medlem.

Alle medlemmer tjener i utgangspunktet det samme for den samme reisen på samme klasse. (Og her stopper likheten mellom oppgaveteksten og velkjente bonusordninger...). Sølvmedlemmer får et påslag i antall poeng på 20%, mens gullmedlemmer får et påslag på 30% på poeng fra og med 75.000 til 90.000, og 50% for poeng over (fra og med) 90.000.