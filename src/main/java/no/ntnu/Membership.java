package no.ntnu;

public abstract class Membership {
    abstract int registerPoints(int bonusPointBalance, int newPoints);
    abstract String getMembershipName();
}