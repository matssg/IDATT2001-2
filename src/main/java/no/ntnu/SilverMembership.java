package no.ntnu;

public class SilverMembership extends Membership{
    private final float POINTS_SCALING_FACTOR = 1.2f;


    public int registerPoints(int bonusPointBalance, int newPoints) {
        int total = Math.round(bonusPointBalance + newPoints*POINTS_SCALING_FACTOR);
        return total;
    }
    
    public String getMembershipName() {
        return "Silver";
    }
}
