package no.ntnu;

public class GoldMembership extends Membership{
    private final float POINTS_SCALING_FACTOR_LEVEL_1 = 1.3f;
    private final float POINTS_SCALING_FACTOR_LEVEL_2 = 1.5f;

    public int registerPoints(int bonusPointBalance, int newPoints) {
        int total;
        if (90000 > bonusPointBalance) {
            total = Math.round(bonusPointBalance + newPoints*POINTS_SCALING_FACTOR_LEVEL_1);
        } else {
            total = Math.round(bonusPointBalance + newPoints*POINTS_SCALING_FACTOR_LEVEL_2);
        }
        return total;
    }
    
    public String getMembershipName() {
        return "Gold";
    }
}
