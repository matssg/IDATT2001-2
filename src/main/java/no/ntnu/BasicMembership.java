package no.ntnu;

public class BasicMembership extends Membership {

    public int registerPoints(int bonusPointBalance, int newPoints) {
        int total = bonusPointBalance + newPoints;
        return total;
    }
    
    public String getMembershipName() {
        return "Basic";
    }
}
