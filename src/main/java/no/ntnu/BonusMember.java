package no.ntnu;

import java.time.LocalDate;

/**
 * The object contains all the data of a member.
 */
public class BonusMember {
    private int memberNumber;
    private LocalDate enrolledDate;
    private int bonusPointsBalance = 0;
    private String name;
    private String eMailAddress;
    private String password = null;

    private Membership membership;

    private static final int SILVER_LIMIT = 25000;
    private static final int GOLD_LIMIT = 75000;

    /**
     * Creates a new bonus-member.
     * @param memberNumber Member number
     * @param enrolledDate Date of enrollment
     * @param bonusPointsBalance Bonus point balance
     * @param name Name
     * @param eMailAdress e-mail adress
     */
    public BonusMember(int memberNumber, LocalDate enrolledDate, int bonusPointsBalance, String name, String eMailAdress) {
        this.memberNumber = memberNumber;
        this.enrolledDate = enrolledDate;
        this.bonusPointsBalance = bonusPointsBalance;
        this.name = name;
        this.eMailAddress = eMailAdress;
        this.checkAndSetMembership();
    }

    /**
     * Creates a new bonus-member.
     * @param memberNumber Member number
     * @param enrolledDate Date of enrollment
     * @param bonusPointsBalance Bonus point balance
     * @param name Name
     * @param eMailAdress e-mail adress
     * @param password Password
     */
    public BonusMember(int memberNumber, LocalDate enrolledDate, int bonusPointsBalance, String name, String eMailAdress, String password) {
        this.memberNumber = memberNumber;
        this.enrolledDate = enrolledDate;
        this.bonusPointsBalance = bonusPointsBalance;
        this.name = name;
        this.eMailAddress = eMailAdress;
        this.password = password;
        this.checkAndSetMembership();
    }

    public int getMemberNumber() {
        return memberNumber;
    }
    public LocalDate getEnrolledDate() {
        return enrolledDate;
    }
    public int getBonusPointsBalance() {
        return bonusPointsBalance;
    }
    public String getName() {
        return name;
    }
    public String geteMailAddress() {
        return eMailAddress;
    }
    public String getPassword() {
        return password;
    }
    public Membership getMembership() {
        return membership;
    }

    /**
     * Checks if the password is correct.
     * @param password
     * @return {@code true} if the password is correct,
     *         {@code false} if the password is wrong.
     */
    public boolean checkPassword(String password) {
        return password.equals(this.getPassword());
    }

    /**
     * Compares current bonus point balance to the membership tier milestones. 
     * If the current balance is high enough changes the tier of the member.
     */
    private void checkAndSetMembership() {
        if (bonusPointsBalance >= GOLD_LIMIT) {
            this.membership = new GoldMembership();
        } else if (bonusPointsBalance >= SILVER_LIMIT) {
            this.membership = new SilverMembership();
        } else {
            this.membership = new BasicMembership();
        }
    }

    /**
     * Checks and adds point to the members balance.
     * @param newPoints Points to be added to the members balance.
     */
    public void registerBonusPoints(int newPoints) {
        this.bonusPointsBalance = this.membership.registerPoints(this.bonusPointsBalance, newPoints);
        this.checkAndSetMembership();
    }

    public String toString() {
        return ("Member: " + getMemberNumber() + "\nName: " + getName() + "\nBonus Points: " + getBonusPointsBalance() + "\neMail: " + geteMailAddress() + "\nEnrolled Date: " + getEnrolledDate() + "\nMembership: " + getMembership().getMembershipName() + "\n");
    }
}
